<script> function rpage(){
window.location.reload();
}
</script>

<h1>Rootless Root</h1>
<h2>The Unix Koans of Mater Foo</h2>
<h5> By Eric Steven Raymond </h5>
<br>
<?php
$db=new mysqli('db', 'root', 'docker', 'demodb');

if($db->connect_erno>0){
die('unable to connect to database ['.$db->connect_error . ']');
}

$sql = 'select * from linxkoan order by rand() limit 1';

if($result = $db->query($sql)){
while($row = $result->fetch_assoc()){
echo "<h3>" . $row['title'] . "</h3><br><p>" . $row['koan'] . "</p>";
}
}
$result->free();
$db->close();

?>

<button type="submit"  onclick="rpage()" >More Enlightenment!</button>
